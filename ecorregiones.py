import os
import pandas as pd

path_regiones = "/home/fidel/GitLab/sample-points/data/sabanas/Ecorregiones_puntos"

lista_shapes = os.listdir(path_regiones)
layer = QgsVectorLayer("/home/fidel/GitLab/sample-points/data/sabanas/ecorregiones.shp", "todos", "ogr")
the_fields = layer.fields()
l_provider = layer.dataProvider()
#layer.beginEditCommand("Feature triangulation")
id_eco = 0
id = 0
ecorregiones = []
for f in sorted(lista_shapes):
    if f[-4:] == ".shp":
        id_eco += 1
        
        nombre = f[:-9]
        ecorregiones.append(nombre)
        print(id_eco,f, nombre)
        f_path = os.path.join(path_regiones,f)
        print(f_path)
        ecorregion = QgsVectorLayer(f_path,nombre, "ogr")
        
        for feature in ecorregion.getFeatures():
            id += 1
            print(id)
            geom = feature.geometry()
            new_f = QgsFeature()
            new_f.setGeometry(geom)
            new_f.setFields(the_fields)
            new_f.setAttribute('id', id)
            #new_f["id"] = id
            #new_f["id_eco"] = id_eco
            new_f.setAttribute('id_eco', id_eco)
            l_provider.addFeatures([new_f])
            
        layer.updateExtents()
        
#layer.endEditCommand()



df = pd.DataFrame({'id_eco': range(1,id_eco+1), 'ecorregion': ecorregiones})

df.to_csv("/home/fidel/GitLab/sample-points/data/sabanas/ecorregiones_ids.csv", index=False)
